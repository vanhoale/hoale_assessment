package org.parrot.assessment.model;

import java.util.List;

/**
 * @author hoa.le
 *
 */
public class Video {
	private int page;
	private List<VideoData> data;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public List<VideoData> getData() {
		return data;
	}
	public void setData(List<VideoData> data) {
		this.data = data;
	}
	
	
}
