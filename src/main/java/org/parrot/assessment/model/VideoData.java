package org.parrot.assessment.model;
/**
 * The model class for Vimeo Video metadata 
 * For purpose of the test, I just get uri (to get comments and likes) and name (title)
 * @author hoa.le
 *
 */
public class VideoData {
	private String uri;
	private String name;
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
