package org.parrot.assessment.model;

import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedTimestamp;

@DynamoDBTable(tableName = "spe-hoa")
public class SpeHoa {
    
    private String title;

    
    private Date timestamp;	
    
    
    private String titleId;
    
    private int likes;
    
    private int comments;
    
    private String videoUri;
    
    @DynamoDBHashKey(attributeName = "title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@DynamoDBRangeKey(attributeName="timestamp")
	@DynamoDBTypeConvertedTimestamp(pattern="yyyyMMddHH", timeZone="UTC") //One hour a record
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@DynamoDBAttribute(attributeName="likes")
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	
	@DynamoDBAttribute(attributeName="comments")
	public int getComments() {
		return comments;
	}
	public void setComments(int comments) {
		this.comments = comments;
	}
	
	@DynamoDBAttribute(attributeName="title_id")
	public String getTitleId() {
		return titleId;
	}
	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}
	
	@DynamoDBAttribute(attributeName="video_uri")
	public String getVideoUri() {
		return videoUri;
	}
	public void setVideoUri(String videoUri) {
		this.videoUri = videoUri;
	}
	public SpeHoa(String title, Date timestamp, String titleId, int likes, int comments, String videoUri) {
		super();
		this.title = title;
		this.timestamp = timestamp;
		this.titleId = titleId;
		this.likes = likes;
		this.comments = comments;
		this.videoUri = videoUri;
	}

    
}
