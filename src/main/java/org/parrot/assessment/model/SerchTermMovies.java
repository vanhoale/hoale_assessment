package org.parrot.assessment.model;

import com.github.davidmoten.rx.jdbc.annotations.Column;

public interface SerchTermMovies {
	@Column("movie_id")
	String movieId();
	@Column("movie_title")
	String movieTitle();
}
