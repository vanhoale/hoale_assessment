package org.parrot.assessment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.parrot.assessment.utils.S3Utils;

public class Consumer implements Runnable {
	private static final Logger logger = Logger.getLogger(Consumer.class);
    private final BlockingQueue<String> queue;
    private String partKey;
    private String ext;
    private static final int NUM_LINE_IN_FILE = 1000;
    private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public void run() {

        try {
        	int i = 0;
            while (true) {
            	if(queue.size() != 0) {
	            	StringBuilder sb = new StringBuilder();
	            	sb.append(queue.take() + "\n");
	            	i++;
	            	if (i==NUM_LINE_IN_FILE) {
	            		logger.info("Writing file to S3...");
	            		S3Utils.putObject(partKey + dateFormat.format(new Date()) + "/" + UUID.randomUUID().toString() + ext, sb.toString());
	            		i=0;
	            	}	            		
            	}

            }
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }

    }

    public Consumer(BlockingQueue<String> queue, String partKey, String ext) {
        this.queue = queue;
        this.partKey = partKey;
        this.ext = ext;
    }
}
