package org.parrot.assessment;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.apache.log4j.Logger;
import org.glassfish.jersey.client.rx.rxjava2.RxFlowableInvoker;
import org.glassfish.jersey.client.rx.rxjava2.RxFlowableInvokerProvider;
import org.parrot.assessment.auth.AddHeaderOnRequestFilter;
import org.parrot.assessment.model.Comments;
import org.parrot.assessment.model.Likes;
import org.parrot.assessment.model.SerchTermMovies;
import org.parrot.assessment.model.SpeHoa;
import org.parrot.assessment.model.Video;
import org.parrot.assessment.model.VideoData;
import org.parrot.assessment.utils.DynamoDBUtils;

import com.github.davidmoten.rx.jdbc.ConnectionProvider;
import com.github.davidmoten.rx.jdbc.ConnectionProviderFromUrl;
import com.github.davidmoten.rx.jdbc.Database;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revinate.guava.util.concurrent.RateLimiter;

import io.reactivex.Flowable;

public class Main {
	private static final Logger logger = Logger.getLogger(Main.class);
	private static String BASE_VIMEO_URI = "https://api.vimeo.com";
	private static String DB_CONNECTION = "jdbc:mysql://pa-consolidated-dbs.cluster-ctkeuweqhlpx.us-east-1.rds.amazonaws.com:3306/parrotplay";
	private static String DB_USER = "parrotplay";
	private static String DB_PASSWORD = "playp3n";
	private static ConnectionProvider connectionProvider = new ConnectionProviderFromUrl(DB_CONNECTION, DB_USER,
			DB_PASSWORD);

	private static Gson GSON = new GsonBuilder().create();
	
	//This sync queue will be read at the end to concat API messages in one and write to S3, one write for one run
	private static final BlockingQueue<String> queue = new LinkedBlockingQueue<>();
	
	private static final BlockingQueue<String> emptySearchTermQueue = new LinkedBlockingQueue<>();
	
	private static final BlockingQueue<String> commnentsQueue = new LinkedBlockingQueue<>();
	
	private static final BlockingQueue<String> likesQueue = new LinkedBlockingQueue<>();
	
	
	//4 requests/ 10 seconds
	private static RateLimiter limiter = RateLimiter.create(0.4);

	// The application is designed to run every hour
	public static void main(String[] args) {
		Instant start = Instant.now();
		new Thread(new Consumer(queue, "spe-hoa/vmieo-videos/", ".json")).start();
		new Thread(new Consumer(emptySearchTermQueue, "spe-hoa/no-terms/", ".txt")).start();
		new Thread(new Consumer(commnentsQueue, "spe-hoa/comments/", ".json")).start();
		new Thread(new Consumer(likesQueue, "spe-hoa/likes/", ".json")).start();
		Database db = null;
		try {
			db = Database.from(connectionProvider);
			logger.info("Getting terms for movies searching");
			List<SerchTermMovies> serchTermMovies = db.select("select movie_id, movie_title from serch_term_movies")
					.autoMap(SerchTermMovies.class).toList().toBlocking().single();
			serchTermMovies.parallelStream().forEach(stm -> process(stm));

			Instant finish = Instant.now();
		    
		    long timeElapsed = Duration.between(start, finish).toMillis();  //in millis	
		    logger.info("Total time for running 194 movie terms: " + timeElapsed/1000 + " seconds");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.close();
			}
		}

	}

	private static void process(SerchTermMovies stm) {
		for (int i = 1; i < 6; i++) {
			getResultByPage(i, stm);
		}
	}

	private static int getResultByPage(int pageNumber, SerchTermMovies stm) {
		Client client = null;
		try {
			client = ClientBuilder.newBuilder().register(AddHeaderOnRequestFilter.class).build();
			client.register(RxFlowableInvokerProvider.class);
			//Video endpoint
			WebTarget videoService = client.target(BASE_VIMEO_URI + "/videos").queryParam("query", stm.movieTitle())
					.queryParam("page", pageNumber);

			String resp = hitVimeoAPI(videoService).blockingFirst();

			
			Video video = GSON.fromJson(resp, Video.class);
			
			List<VideoData> datas = video.getData();
			//I prefer to produce a message here to kafka/kinesis stream since the application memory is limited 
			// And then data will be consume from stream using spark streaming and write to S3.
			// but for this scope of the assessment we can use BlockingQueue			
			if(datas == null || datas.size()==0) {
				//Trigger here to get the search terms with no response
				emptySearchTermQueue.put(stm.movieTitle());
			} else {
				queue.put(resp);
			}
			datas.parallelStream().forEach(vd -> processVideoData(vd, stm));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				client.close();
			}
		}

		return pageNumber;
	}
	
	private static void processVideoData(VideoData vd, SerchTermMovies stm) {
		Client client = null;
		try {
			client = ClientBuilder.newBuilder().register(AddHeaderOnRequestFilter.class).build();
			client.register(RxFlowableInvokerProvider.class);
			
			//Comments endpoint
			WebTarget commentSrv = client.target(BASE_VIMEO_URI + vd.getUri() + "/comments");
			String resp = hitVimeoAPI(commentSrv).blockingFirst();	
			commnentsQueue.put(resp);
			Comments comments = GSON.fromJson(resp, Comments.class);
			
			//Likes endpoint
			WebTarget likesSrv = client.target(BASE_VIMEO_URI + vd.getUri() + "/likes");
			String likeResp = hitVimeoAPI(likesSrv).blockingFirst();
			likesQueue.put(likeResp);
			Likes likes = GSON.fromJson(likeResp, Likes.class);		
			
			//Save to DynamoDB
			DynamoDBUtils.putItem(new SpeHoa(vd.getName(), new Date(), stm.movieId(), likes.getTotal(), comments.getTotal(), vd.getUri()));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				client.close();
			}
		}		
	}
	
	private static Flowable<String> hitVimeoAPI(WebTarget service) throws Exception{
		limiter.acquire();	
		logger.info("Getting data from Vimeo API");
		return service.request().rx(RxFlowableInvoker.class).get(String.class).doOnError(throwable -> new RuntimeException(throwable.getMessage()));		
	}

}
