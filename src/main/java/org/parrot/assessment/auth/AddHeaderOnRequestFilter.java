package org.parrot.assessment.auth;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

public class AddHeaderOnRequestFilter implements ClientRequestFilter {
	 
    public static final String FILTER_HEADER_VALUE = "basic YjMwMWQzNDZhOWU5YjUyYTRhMDFmMjk0MzJjZTUxOGY1NjM4NWU5YTpHaVdZMTJoMXVNc25FU3FLZEFuWmdXRmtQR0NKWmZEWEJMSHplUDB4UU1meXFRQlEyUElrMmJZVkpkZ1JJTmhDQzltZU1tcXBJUnJlRVZFM2F1QzExWVYvVit1bXdWUmdFcEJZY2ZpRnlXczVkRUp3UnR2NEZBdnJobnBqcWxXMw==";
    public static final String FILTER_HEADER_KEY = "Authorization";
 
    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        requestContext.getHeaders().add(FILTER_HEADER_KEY, FILTER_HEADER_VALUE);
    }
}
