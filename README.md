Design:

1. The search terms are fetched from serch_term_movies table (it's 194 rows)

2. Based on movie title, build the query search to https://api.vimeo.com/videos?query=<Movie_Title>

3. One search result may have multiple pages, only take from page 1 to page 5

4. Parse json for each page (using Gson for json parser)

5. Each page will have multiple movies (each one is at uri: "uri": "/videos/{video_id}",)

6. Take uri to get comments (https://api.vimeo.com/videos/{video_id}/comments) and to get likes (https://api.vimeo.com/videos/{video_id}/likes)

7. Check if video data response is empty then write to Non-result-return-terms queue to do manually validation

8. If video data responses then queue up to out put queue for writing to S3 later

9. DynamoDB data will be combined by serch_term_movies(movie_id), comments count, likes count, video_uri, movie title

10. For scalability:
	
	10.1. Vertical scale: it's using Java 8 lambda feature for parallelStream and Async call from RxJava2 library, but it looks like not support much because of rate limit.
	
	10.2. Horizontal scale: We can break down the source from MySQL or serch_term_movies to multiple offset and then contributed to multiple java containers (the akka-scala can handle this very well) 
    
	10.3. Using kafka/kinesis to store raw json that will be consumed by Spark streaming, and then transform to parquet or raw json to S3
    
    
11. The rate limit is about 25 requests/ minute, so I have to set RateLimiter (a google guava library) to 0.4 to run application without error.
We should think about increasing rate limit to support scaling. 
I see some Vimeo APIs are supporting batch call, but not having time to look at it yet. That's also possible to increase performance.

12. I can guess 20k of movie search terms will take 100 times longer with current rate limit